import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    StyleSheet, 
    Image,
    ScrollView
} from 'react-native';

 

class Kooli extends React.Component{

  constructor(props) {
    super(props) 
    this.state = {
      
    };
  }

componentDidMount() {
  return fetch('http://www.kooli.no:8000/api/products')
    .then(d => d.json())
    .then( d => {
      this.setState({
        products: d

       })
    })
}

  render() {

    if (!this.state.products) 
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>  
        <Text>Loading...</Text>
      </View>
      )



     
      return (  <ScrollView>
        {this.state.products.map(product =>
         <View key={product.id} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          
          <Image source={{uri: product.images[0].src}}
          style={{width: 400, height: 200}} />
          <Text>{product.name}</Text>
          </View>
          )}
        </ScrollView>
      )
      

  }
}

export default Kooli;